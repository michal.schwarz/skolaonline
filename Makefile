all:	modules

modules:
	cpanm --installdeps .
	carton install

deploy: modules
	./private/deploy
	
update_db_schema:
	carton exec ./scripts/dbic_dump.sh
