package SOL::Changes::Change;

use strict;
use utf8;

use Moose;
use MooseX::StrictConstructor;
use DDP;

has date             => (is => 'ro', isa => 'Maybe[DateTime]');
has course           => (is => 'ro', isa => 'Maybe[Str]');
has topic            => (is => 'ro', isa => 'Maybe[Str]');
has old_score        => (is => 'ro', isa => 'Maybe[Num]');
has old_verbal_score => (is => 'ro', isa => 'Maybe[Str]');
has old_weight       => (is => 'ro', isa => 'Maybe[Num]');
has new_score        => (is => 'ro', isa => 'Maybe[Num]');
has new_verbal_score => (is => 'ro', isa => 'Maybe[Str]');
has new_weight       => (is => 'ro', isa => 'Maybe[Num]');

1;