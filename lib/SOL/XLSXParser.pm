package SOL::XLSXParser;

use strict;
use utf8;

use Moose;
use DDP;
use Path::Class;
use Spreadsheet::ParseXLSX;
use DateTime;
use DateTime::Format::Excel;
use Sort::Key::Maker exceldata_keysort => qw(string string string number string number);

use SOL::Changes;
use SOL::Changes::Change;

with qw/SOL::Role::Config SOL::Role::Schema/;

has changes => (is => 'ro', lazy_build => 1);

sub _build_changes {
    my $self = shift;

    return SOL::Changes->new;
}

sub process_all_files_in_data_folder {
    my $self = shift;

    my $dir = Path::Class::Dir->new($self->config->{export}->{dir});
    while (my $file = $dir->next) {

        # $file is a Path::Class::File or Path::Class::Dir object
        next unless -f $file;
        next unless $file->basename =~ /\.xlsx$/;

        $self->process_single_xlsx($file);
    }

    return;
}

sub process_single_xlsx {
    my $self  = shift;
    my $file  = shift;
    my $force = shift;

    my $dbfile = $self->schema->resultset('FileSeen')->search({ name => $file->basename })->first();
    if ($dbfile) {
        return if !$force;
    } else {
        warn "creating new DB record for file " . $file->basename;
        $dbfile = $self->schema->resultset('FileSeen')->create(
            {
                name       => $file->basename,
                first_seen => DateTime->now(),
            }
        );
    }

    warn "processing new file $file (id=" . $dbfile->id . ')';
    my $parser = Spreadsheet::ParseXLSX->new;

    #warn $parser;
    my $workbook = $parser->parse($file->open('r'));

    #warn $workbook;

    my @raw_excel_rows = ();
    for my $worksheet ($workbook->worksheets()) {
        my ($row_min, $row_max) = $worksheet->row_range();

        #my ($col_min, $col_max) = $worksheet->col_range();

        # Datum, Predmet, Tema, Vaha, Vysledek, Slovni_hodnoceni
        # <date>, <string>, <string>, <float>, <float|'-'>, <string>

        for my $row ($row_min + 1 .. $row_max) {    # skip the header

            #print "row=$row"
            # for my $col (0 .. 5) {    # $col_min .. $col_max) {

            #     my $cell = $worksheet->get_cell($row, $col);
            #     next unless $cell;

            #     print "Row, Col    = ($row, $col)\n";
            #     print "Value       = ", $cell->value(),       "\n";
            #     print "Unformatted = ", $cell->unformatted(), "\n";
            #     print "\n";
            # }

            my %new = (
                date_valid_from => DateTime::Format::Excel->parse_datetime($worksheet->get_cell($row, 0)->unformatted),
                course          => $worksheet->get_cell($row, 1)->value,
                topic           => $worksheet->get_cell($row, 2)->value,
                weight          => $worksheet->get_cell($row, 3)->value,
                score           => $worksheet->get_cell($row, 4)->value,
            );
            my $descr = $worksheet->get_cell($row, 5);
            if ($descr) {
                $new{verbal_score} = $descr->value;
            }
            if ($new{score} !~ /^\d+$/) {
                warn "Non-numeric score: [$new{score}]";
                $new{score} = undef;
            }
            push @raw_excel_rows, \%new;
        }
    }

    exceldata_keysort_inplace { $_->{date_valid_from}, $_->{course}, $_->{topic}, $_->{score}, $_->{verbal_score}, $_->{weight} } @raw_excel_rows;

    my %already_seen = ();
    foreach my $excel_row (@raw_excel_rows) {
        my %new = %{$excel_row};

        # There might occasionally be multiple rows with the same date, course and topic but with a different score or verbal score
        my $tmpkey = join('|', $new{date_valid_from}, $new{course}, $new{topic});
        if ($already_seen{$tmpkey}) {
            $already_seen{$tmpkey}++;
            warn "Multiple rows with the same date, course and topic: $tmpkey";
            $new{topic} .= ' (' . $already_seen{$tmpkey} . ')';
        } else {
            $already_seen{$tmpkey} = 1;
        }

        my $dtf = $self->schema->storage->datetime_parser;
        my $row = $self->schema->resultset('Classification')->find(
            {
                date_valid_from => $dtf->format_date($new{date_valid_from}),
                course          => $new{course},
                topic           => $new{topic},
                deleted         => 0,
            }
        );

        if ($row) {

            # row already exists
            $row->date_last_seen(DateTime->now());

            if ($row->weight != $new{weight} || $row->score != $new{score} || $row->verbal_score ne $new{verbal_score}) {
                warn $row->id . ': existing score has changed!';

                $self->changes->add_change(
                    SOL::Changes::Change->new(
                        date             => $row->date_valid_from,
                        course           => $row->course,
                        topic            => $row->topic,
                        old_score        => $row->score,
                        old_verbal_score => $row->verbal_score,
                        old_weight       => $row->weight,
                        new_score        => $new{score},
                        new_verbal_score => $new{verbal_score},
                        new_weight       => $new{weight},
                    )
                );

                $row->weight($new{weight});
                $row->score($new{score});
                $row->verbal_score($new{verbal_score});
                $row->file_id($dbfile->id);    # update the file_id only when some data changes, not on every occurrence
            }

            $row->update();
        } else {
            warn "inserting new row";

            #p %new;

            my $row = $self->schema->resultset('Classification')->new(
                {
                    #
                    date_first_seen => DateTime->now(),
                    date_last_seen  => DateTime->now(),
                    file_id         => $dbfile->id,
                    %new,
                }
            )->insert()->discard_changes();    # load fresh data from database after insert (mainly the dates)

            $self->changes->add_change(
                SOL::Changes::Change->new(
                    date             => $row->date_valid_from,
                    course           => $row->course,
                    topic            => $row->topic,
                    old_score        => undef,
                    old_verbal_score => undef,
                    old_weight       => undef,
                    new_score        => $row->score,
                    new_verbal_score => $row->verbal_score,
                    new_weight       => $row->weight,
                )
            );
        }
    }

    return;
}

1;
