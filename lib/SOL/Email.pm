package SOL::Email;

use strict;
use utf8;

use Moose;
use DDP;
use Encode;
use Email::Sender::Simple qw(sendmail);
use Email::Sender::Transport::SMTP ();
use Email::Simple                  ();
use Email::Simple::Markdown        ();
use Email::Simple::Creator         ();
use MIME::Lite;
use CSS::Inliner;
use Template;

use SOL::Changes;

with qw/SOL::Role::Config/;

sub generate_email {
    my $self    = shift;
    my $changes = shift;

    my $tt = Template->new({
            ENCODING     => 'utf8',
            INCLUDE_PATH => [
                #
                $self->app_dir . '/templates/locale/' . $self->config->{lang},
                $self->app_dir . '/templates/locale/en_US',
                $self->app_dir . '/templates/',
            ],
        }) || die "$Template::ERROR";

    my $vars = {
        count_changes => $changes->count_changes,
        changes       => [ $changes->list_changes ],
    };

    # markdown templates
    my $body;
    my $subject;
    $tt->process('email_body.tt',    $vars, \$body)    || die $tt->error();
    $tt->process('email_subject.tt', $vars, \$subject) || die $tt->error();

    my $inliner = new CSS::Inliner();
    $inliner->read({html => $body});
#    warn $inliner->inlinify(); die;

    my $email = MIME::Lite->new(
        From     => $self->config->{smtp}->{from},
        To       => $self->config->{smtp}->{to},
        Type     => 'text/html',
        Encoding => 'quoted-printable',
        Subject  => encode('MIME-Q', $subject),    # convert to quoted-printable (handles internal utf-8 nicely, too)
        #Data     => encode('utf8',   $body),       # it doesn't like native utf8 strings
        Data     => encode('utf8',   $inliner->inlinify()),       # it doesn't like native utf8 strings
    );
    $email->attr('content-type.charset' => 'UTF-8');

    #die $email->as_string;
    return $email;
}

sub send_email {
    my $self    = shift;
    my $changes = shift;

    warn "emailing changes: ";

    my $smtpserver   = $self->config->{smtp}->{server};
    my $smtpport     = $self->config->{smtp}->{port};
    my $smtpuser     = $self->config->{smtp}->{username};
    my $smtppassword = $self->config->{smtp}->{password};

    my $transport = Email::Sender::Transport::SMTP->new({
            host => $smtpserver,
            port => $smtpport,

            #ssl           => "starttls",	# not on localhost
            sasl_username => $smtpuser,
            sasl_password => $smtppassword,

            #debug => 1,    # FIXME
    });

    my $email = $self->generate_email($changes);
    warn "sending this email:\n" . $email->as_string;
    sendmail($email->as_string, { transport => $transport });

    warn "OK";

    return;
}

1;
