use utf8;
package SOL::Schema::Result::Classification;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

SOL::Schema::Result::Classification

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<classification>

=cut

__PACKAGE__->table("classification");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0

=head2 file_id

  data_type: 'int'
  is_nullable: 1

=head2 date_first_seen

  data_type: 'timestamp'
  is_nullable: 0

=head2 date_last_seen

  data_type: 'timestamp'
  is_nullable: 0

=head2 deleted

  data_type: 'int'
  default_value: 0
  is_nullable: 0

=head2 date_valid_from

  data_type: 'date'
  is_nullable: 0

=head2 course

  data_type: 'text'
  is_nullable: 1

=head2 topic

  data_type: 'text'
  is_nullable: 1

=head2 weight

  data_type: 'numeric'
  is_nullable: 1
  size: [5,2]

=head2 score

  data_type: 'int'
  is_nullable: 1

=head2 verbal_score

  data_type: 'text'
  is_nullable: 1

=head2 changed

  data_type: 'int'
  default_value: 0
  is_nullable: 0

=head2 _drop1

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_auto_increment => 1, is_nullable => 0 },
  "file_id",
  { data_type => "int", is_nullable => 1 },
  "date_first_seen",
  { data_type => "timestamp", is_nullable => 0 },
  "date_last_seen",
  { data_type => "timestamp", is_nullable => 0 },
  "deleted",
  { data_type => "int", default_value => 0, is_nullable => 0 },
  "date_valid_from",
  { data_type => "date", is_nullable => 0 },
  "course",
  { data_type => "text", is_nullable => 1 },
  "topic",
  { data_type => "text", is_nullable => 1 },
  "weight",
  { data_type => "numeric", is_nullable => 1, size => [5, 2] },
  "score",
  { data_type => "int", is_nullable => 1 },
  "verbal_score",
  { data_type => "text", is_nullable => 1 },
  "changed",
  { data_type => "int", default_value => 0, is_nullable => 0 },
  "_drop1",
  { data_type => "text", is_nullable => 1 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-11-11 19:12:20
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Bwmi7rWdk89BmI04jPjEhg

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
