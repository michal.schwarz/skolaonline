package SOL::Integration::SOLWebPage;

use strict;
use utf8;

use Encode;
use Moose;
use WWW::Mechanize qw();
use LWP::ConsoleLogger::Easy qw( debug_ua );
use DDP;

with qw/SOL::Role::Config/;

has base_url => (is => 'ro', default => 'https://aplikace.skolaonline.cz/');

sub get_results_as_excel_workbook {
    my $self = shift;

    # Enable strict form processing to catch typos and non-existant form fields.
    my $mech = WWW::Mechanize->new();    # strict_forms => 1);

    # Debugging:
    #
    # my $console_logger = debug_ua($mech);
    # $console_logger->dump_content(0);
    # $console_logger->dump_text(0);
    # $console_logger->dump_title(0);
    # $console_logger->dump_cookies(0);

    # create session cookie
    $mech->get($self->base_url);

    # login
    my $response = $mech->post(
        $self->base_url . 'SOL/Prihlaseni.aspx',
        {
            JmenoUzivatele => $self->config->{web}->{login}->{username},
            HesloUzivatele => $self->config->{web}->{login}->{password},
        });

    # get the page with XLS export button
    $mech->get($self->base_url . 'SOL/App/Hodnoceni/KZH003_PrubezneHodnoceni.aspx#');    

    # fetch the XLS
    $mech->set_fields(
        'ctl00$main$ddlObdobi'        => $self->config->{web}->{period},
        'ctl00$main$ddlPredmet'       => '',
        'gridHodnoceni_DownloadName'  => 'PodepsaniHodnoceni',
        'gridHodnoceni_WorksheetName' => '',
        'gridHodnoceni_Rozsah'        => 'VSE',
        'gridHodnoceni_Formatovani'   => 'NE',
        'gridHodnoceni_VerzeExcelu'   => 'XLSX',
    );
    $mech->click('gridHodnoceni_BExport');

    die "unexpected response (content-type): " . $mech->response->headers_as_string
        if $mech->response->header('Content-Type') !~ /excel|spreadsheet/i;
        # Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8

    return $mech->content;
}

1;