package SOL::Config;

use strict;
use utf8;

use Cwd qw(abs_path);
use File::Basename qw(dirname);
use Moose;
use YAML::Syck qw();
use Hash::Util qw();

has config => (is => 'ro', lazy_build => 1);
has app_dir => (is => 'ro', lazy_build => 1);

# directory where the whole application resides
sub _build_app_dir {
    my $self = shift;
    my $module_dir = abs_path(dirname(__FILE__));
    my $app_dir = abs_path($module_dir . '/../..');
    return $app_dir;
}

sub _build_config {
    my $self = shift;

    my $config_file = $self->app_dir . '/sol.yaml';

    my $data = YAML::Syck::LoadFile($config_file);
    Hash::Util::lock_hashref_recurse($data);

    return $data;
};

1;

