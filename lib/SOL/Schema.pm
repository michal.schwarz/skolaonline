use utf8;
package SOL::Schema;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2022-05-17 20:27:48
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:ADhwVAR3wFa7chEozss4kA


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
