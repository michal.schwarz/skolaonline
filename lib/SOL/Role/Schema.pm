package SOL::Role::Schema;

use strict;
use utf8;

use Moose::Role;
use SOL::Schema;

with qw/SOL::Role::Config/;

has schema => (is => 'ro', lazy_build => 1);

sub _build_schema {
    my $self = shift;

    my $schema = SOL::Schema->connect(
        #
        $self->config->{database}->{dsn},
        $self->config->{database}->{username},
        $self->config->{database}->{password},
        {
            sqlite_unicode => 1
        }) || die;

    #$schema->storage->debug(1);    # FIXME tmp
    return $schema;
}

1;