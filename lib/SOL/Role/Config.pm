package SOL::Role::Config;

use strict;
use utf8;

use Moose::Role;
use SOL::Config;

has config => (is => 'ro', lazy_build => 1);
has app_dir => (is => 'ro', lazy_build => 1);

sub _build_config {
    my $self = shift;

    return SOL::Config->new->config;
}

sub _build_app_dir {
    my $self = shift;

    return SOL::Config->new->app_dir;
}

1;