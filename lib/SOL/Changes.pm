package SOL::Changes;

use strict;
use utf8;

use Moose;
use DDP;

with qw/SOL::Role::Config/;

has data => (
    is      => 'ro',
    isa     => 'ArrayRef[SOL::Changes::Change]',
    traits  => [qw/Array/],
    default => sub { [] },
    handles => {
        add_change    => 'push',
        count_changes => 'count',
        list_changes  => 'elements',
    });

1;