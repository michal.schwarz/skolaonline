# Automatic classification extractor from Skola Online website

This is a set of perl scripts to automate downloading of XLSX classification sheet from `skolaonline.cz` website, parse it and detect changes. 

Reasoning behind this is the fact that the classification is often entered retrospectively (making the date unusable for quick searches and finding news) and there is no reliable or easy way to find out about changes. Or at least that was the status prior to 5/2022.

## Sample usage:

```bash
./scripts/download_daily_data.pl
./scripts/parse_xls_files.pl
```

### Output email with Czech localization:

![example email](img/2022-05-20-18-23-51.png)