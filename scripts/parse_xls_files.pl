#!/usr/bin/env perl
use strict;
use utf8;

use FindBin qw($Bin);
use lib "$Bin/../lib";

###################################################
package My::App;

use open qw/:std :utf8/;
use Moose;
use DBI;
use DDP;
use Path::Class;

use SOL::XLSXParser;
use SOL::Email;

use Moose;

with 'MooseX::Getopt';

has send_empty_notification => (is => 'rw', isa => 'Bool', required => 0, default => 1);
has filename => (is => 'ro', isa => 'Str', required => 0);

sub run {
    my $self = shift;

    my $parser = SOL::XLSXParser->new;
    {
        # do everything in a single transaction
        my $guard = $parser->schema->txn_scope_guard;

        if ($self->filename) {
            $parser->process_single_xlsx(Path::Class::File->new($self->filename), 1);
        } else {
            $parser->process_all_files_in_data_folder;
        }

        if ($parser->changes->count_changes > 0 || $self->send_empty_notification) {
            my $email = SOL::Email->new;
            $email->send_email($parser->changes);
        }

        $guard->commit();
    }
}

###################################################
package main;

my $app = My::App->new_with_options();
$app->run;
