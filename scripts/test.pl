#!/usr/bin/env perl
use strict;
use utf8;

use open qw/:std :utf8/;
use Moose;
use DBI;
use DDP;
use DateTime;

use lib './lib';
use SOL::Email;
use SOL::Changes;
use SOL::Changes::Change;

my $changes = SOL::Changes->new;
my $email = SOL::Email->new;

$changes->add_change(
    SOL::Changes::Change->new(
        date       => DateTime->now,
        course     => 'Nějaký předmět',
        topic      => 'Zkouška',
        old_score  => 1,
        old_weight => 0.5,
        new_score  => 5,
        new_weight => 1.0,
    ));

$changes->add_change(
    SOL::Changes::Change->new(
        date       => DateTime->now->subtract(days => 5),
        course     => 'Hudební výchova',
        topic      => 'Pololetní písemka',
        new_score  => 1,
        new_weight => 0.25,
    ));

$changes->add_change(
    SOL::Changes::Change->new(
        date       => DateTime->now,
        course     => 'Levitace',
        topic      => '5 minut ve vzduchu, rozcvička',
        new_score  => 2,
        new_weight => 0.5,
    ));

$changes->add_change(
    SOL::Changes::Change->new(
        date       => DateTime->now,
        course     => 'Obrana',
        topic      => 'Obrana proti útočníkovi s banánem',
        new_score  => 3,
        new_weight => 0.5,
    ));


my $mail_data = $email->generate_email($changes);
#use DDP;
#p $email;

print $mail_data->as_string;

#$email->send_email($changes);

