#!/usr/bin/env perl
use strict;
use utf8;

use FindBin qw($Bin);
use lib "$Bin/../lib";

use open qw/:std :utf8/;
use DDP;
use Path::Class;
use DateTime;
use File::Slurp;

use SOL::Integration::SOLWebPage;


my $wp = SOL::Integration::SOLWebPage->new;

my $dir = Path::Class::Dir->new($wp->config->{export}->{dir});
my $fname = $dir->file(DateTime->now()->rfc3339(). '.xlsx');

my $xlsx_data = $wp->get_results_as_excel_workbook;
write_file($fname, $xlsx_data);

warn "Saved as $fname";



