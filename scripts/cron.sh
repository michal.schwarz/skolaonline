#!/bin/bash

BASEDIR=$(dirname "$0")
cd $BASEDIR

QUIET=1
carton exec ./download_daily_data.pl && carton exec ./parse_xls_files.pl "$@"

