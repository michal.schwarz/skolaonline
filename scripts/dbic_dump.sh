#!/usr/bin/bash

dbicdump -o dump_directory=./lib \
         -o components='["InflateColumn::DateTime"]' \
         -o debug=0 \
         SOL::Schema \
         'dbi:SQLite:dbname=db/skolaonline.sqlite'

#         -o overwrite_modifications=1 \