PRAGMA foreign_keys = OFF;
BEGIN TRANSACTION;
CREATE TABLE files_seen (
    id integer primary key autoincrement,
    name text not null unique,
    first_seen timestamp
);
CREATE TABLE classification (
    id integer primary key autoincrement,
    file_id int,
    date_first_seen timestamp not null,
    date_last_seen timestamp not null,
    deleted int not null default '0',
    date_valid_from date not null,
    course text,
    topic text,
    weight numeric(5, 2),
    score int,
    verbal_score text,
    changed int not null default 0
);
COMMIT;